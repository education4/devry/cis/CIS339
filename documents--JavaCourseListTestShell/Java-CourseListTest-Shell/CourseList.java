import java.util.Map;

class CourseList
{
    public Map<String, Course> courseMap = Map.of(
        "CIS 400", new Course ("CIS 400", "OO Analysis & Design", 4, "Important class", "CIS 110") ,
        "CIS 150A", new Course ("CIS 150A" , "VB.NET Programming", 4, "Good Introduction to programming", "CIS 100") ,
        "CIS 150B", new Course ("CIS 150B", "C# Programming with labs", 4, "Follow-up to CIS 100", "CIS 100")
    );


    public Course GetCourseByCourseID(String courseID)
    {
        return courseMap.get(courseID);
    }
}