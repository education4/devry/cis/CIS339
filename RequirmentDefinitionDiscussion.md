# Requirements Definition Discussion

## Preamble

My opinions come from reading most of the book assigned in CIS336 and viewing many talks from a variety of different conferences, e.g. Game Developers Conference, LeadDev, and DEFcon. While this is a lot of theory, it is just that. My opinions have never had to be tested by real life issues, so I have no idea what shortcoming they may have. Also, it should be kept in mind than any model of reality is not comprehensive. There will be situations in which the model fails to aid in furthering your understanding and is detrimental.

## Discussion

When discussing requirements I have three key categories who should have input:

1. Finance - who is paying for the project
2. Maintenance - who is responsible for ensuring the continuation of the project
3. End User - who is expected to use the project

These categories are not exclusive. An individual can be a part of any number of these categories. They are intended merely as a tool to form as comprehensive a list as possible.
